<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Hash;
use Illuminate\Support\Facades\Validator;
 use Input;
 use DB;
// use Illuminate\Support\Facades\Redirect;

use Session;
use Auth;
use Redirect;
use App\User;
use App\Manufacturer;
use App\Dealer;
use Carbon\Carbon;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        
       //$a=Hash::make('admin');
       //echo $a;
        return view('admin.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


    public function login()
    {
        $input=Input::all();
        $validate=Validator::make(Input::all(),
            [
            'uname' => 'required',
            'password' => 'required'
            ]);
        if(!$validate->fails())
        {
            
            $userdata=array('name'=> Input::get('uname'),
            'password' => Input::get('password'));

            $matchThese = ['name' => $userdata['name'], 'user_type_id'=>1];
            $user = User::select('*')->where($matchThese)->first();
            if (Auth::attempt($userdata))
              {
                   $user = Auth::user();
                   $id= Auth::id();
                    if($user->user_type_id == 1)
                     {
                        return view('admin.layout.dashboard')->with('success','login success');
                     }
              }
            else
              {
                     return view('admin.login')->with('error','invalid username or password');
              }
                     
        }
            

    }

    public function insert()
    {

    }


    public function usersindex()
    {
    
      $id=Auth::id();
      $matchThese = ['user_id'=>$id,'user_type_id'=>2];
      $user_list=DB::table('users')->select('*')->where($matchThese)->get();
      return view('admin.users.index',['user_list'=> $user_list]);
    }


    public function manufacturersindex()
    {

        $id=Auth::id();
        $man_list=DB::table('manufacturers')->select('*')->where('user_id',$id)->get();
        return view('admin.manufacturers.index',['man_list'=>$man_list]);
    }
    

    public function productsindex()
    {

        $id=Auth::id();
        $matchThese=['user_id' =>2,'user_type_id'=>$id];
        $pro_list=DB::table('products')->select('*')->where($matchThese)->get();
        return view('admin.products.index',['pro_list'=>$pro_list]);
    }

    public function dealersindex()
    {

        $id=Auth::id();
        $d_list=DB::table('dealers')->select('*')->where('user_id',$id)->get();
        return view('admin.dealers.index',['d_list'=>$d_list]);
    }


    public function userseditview($id)
    {

       $user=DB::table('users')->select('*')->where('id',$id)->get();
       return view('admin.users.editview',['users'=>$user]);
    }


    public function adduserview()
    {
       return view('admin.users.adduser');
    }

    public function adminadddealers_csv()
    {

       return view('admin.dealers.add_csv');
    }

    public function adddealers()
    {

      return view('admin.dealers.add');
    }

    public function store_dealers_csv()
    {
         $file     = Input::all();
         $csv      = Input::file('csvfile')->getClientOriginalName();
         $rules    = array('csvfile' => 'required' );
         $validate = Validator::make(Input::all(),[
          'csvfile' => 'required'
          ]);
         if(!$validate->fails())
         {
          if(Input::hasFile('csvfile'))
           {
            
            $dest      = public_path() . '/uploads/dealers';
            $extension = Input::file('csvfile')->getClientOriginalExtension();
            $filename  = $csv.rand(11111,99999).".".$extension;
            Input::file('csvfile')->move($dest,$filename);
            $this->importdealers($filename);

           
           }

         }

    }

    public function store_manufacturers_xsl()
    {
         $file     = Input::all();
         $csv      = Input::file('xslfile')->getClientOriginalName();
         $rules    = array('xslfile' => 'required' );
         $validate = Validator::make(Input::all(),[
          'xslfile' => 'required'
          ]);
         if(!$validate->fails())
         {
          if(Input::hasFile('xslfile'))
           {
            
            $dest      = public_path() . '/uploads';
            $extension = Input::file('xslfile')->getClientOriginalExtension();
            $filename  = $csv.rand(11111,99999).".".$extension;
            Input::file('xslfile')->move($dest,$filename);
            $this->importmanufactures($filename);

           
           }

         }

    }



    public function adminupdateuser($id)
    {
    
       $input=Input::all();
       $validate=Validator::make(Input::all(),[
       'name' =>'required',
       'email' =>'required|email',
       'password' =>'required|min:6',
       'cpass' =>'required|same:password',
       'info' =>'required'
       ]);
    

       if(!$validate->fails())
        {
        
          $user=User::findOrFail($id);
          $user->name=Input::get('name');
          $user->email=Input::get('email');
          $pa=Input::get('password');
          $p=Hash::make($pa);
          $user->password=$p;
          $user->info=Input::get('info');
          $user->user_id=Auth::id();
          $user->user_type_id=2;
          $user->save();
          echo "success";
        
        }

    }


    public function importmanufactures($file)
    {
       $path = public_path()."/uploads/manufacturers".$file;
      \Excel::load($file, function($reader) use($file) 
      {

         $results= $reader->get()->toArray();
         //echo "<pre>";
         //print_r($results);
         foreach($results as $row)
         {
            $man=new Manufacturer;
            $man->name=$row['name'];
            $man->uploadfilename=$file;
            $man->info=$row['info'];
            $man->user_id=Auth::id();
            $man->created_at=$row['created_at'];
            $man->updated_at=$row['updated_at'];
            $man->save();
            
         }
         echo "success";
      
      });

    }
    


    public function importdealers($file)
    {
      //echo public_path()."/uploads/dealers/".$file;
      //exit;

      \Excel::load("public/uploads/dealers/".$file,function($reader) use($file)
      {
        
        date_default_timezone_set("Asia/Calcutta");
        $results=$reader->get()->toArray();
        foreach ($results as $row) 
        {
          $dealer=new Dealer;
          $dealer->name=$row['name'];
          $dealer->user_id=Auth::id();
          //echo Auth::id();
          exit;
          $dealer->info=$row['info'];
          $dealer->created_at= Carbon::parse($row['created_at']);
          $dealer->updated_at= Carbon::parse($row['updated_at']);
          //$dealer->created_at=Carbon::createFromFormat('Y-m-d H:i:s', $row['created_at'])->copy()->tz(Auth::user()->timezone);
          //$dealer->updated_at=Carbon::createFromFormat('Y-m-d H:i:s', $row['updated_at'])->copy()->tz(Auth::user()->timezone);
          $dealer->uploadfilename=$file;
          $dealer->save();

         }
        
      
      });
      echo "success";

      
    }
    


}

