@extends('admin.layout.dashboard')
@section('content')
		{!! csrf_field() !!}
{!! Form::open(array('method' => 'post', 'url' => 'admin/dealers/store_csv','files'=>'true')) !!}

{!! Form::label('Upload Filename') !!}

{!! Form::file('csvfile') !!}

{!! Form::submit ('Submit',array('name' => 'submit')) !!}

{!! Form::close() !!}
@stop