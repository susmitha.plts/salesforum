@extends('admin.layout.dashboard')
@section('content')

<div class="main-content">
		<div class="main-content-inner">
			<!-- #section:basics/content.breadcrumbs -->
			<div class="breadcrumbs" id="breadcrumbs">
				
				
				<!-- /.breadcrumb -->
				<!-- #section:basics/content.searchbox -->
			<div class="nav-search" id="nav-search">
				
						{!! csrf_field() !!}
						
				</div>
				<!-- /section:basics/content.searchbox -->
			</div><!-- /.nav-search -->
				

			<!-- /section:basics/content.breadcrumbs -->
			<div class="page-content">
				<!-- #section:settings.box -->
				<!-- /.ace-settings-container -->
				<!-- /section:settings.box -->
				
				<div class="row">
					<div class="col-xs-12">
						<!-- Notifications -->
				
						<!-- ./ notifications -->
						<!-- PAGE CONTENT BEGINS -->
						<div class="row">
	                        <div class="col-xs-12 " >
									<table id="simple-table" class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
		                                            <th>#</th>
		                                            <th>Name</th>
		                                            <th>Bio</th>
		                                            <th>Created</th>
		                                            <th>Modified</th>
		                                            <th>Edit</th>
												    <th>Delete</th>
												</tr>
											</thead>

											<tbody>
												@foreach ($d_list as $dealer)
	                                        	<tr>
		                                            <td class="hidden-480">{{{ $dealer->id }}}</td>
		                                            <td>{{{ $dealer->name }}}</td>
		                                            <td>{{{ $dealer->info }}}</td>
		                                            <td>{{{ $dealer->created_at }}}</td>
		                                            <td>{{{ $dealer->updated_at }}}</td>
		                                            <td><a href="{{url('admin/'.$dealer->id.'/adminmdealerseditview')}}"><button  type="buttton" class="btn btn-success">Edit</button></a></td>
		                                            <td><a href="{{url('admin/'.$dealer->id.'/admindealersdelete')}}"><button type="button" class="btn btn-danger">Delete</button></a></td>
</td>
		                                           
		                                             
		                                            
		                                        </tr>
	                                        @endforeach

											</tbody>
										</table>
	                                 
	                                 <div>
			                        	<a href="{{ URL::to('admin/dealers/add') }}" class="btn btn-info btn-sm pull-right">Add New Dealer</a>
			                    	</div> 
	                            </div><!-- /.box -->
	                        </div>
							</div>
							<!-- PAGE CONTENT ENDS -->
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.page-content -->
		</div>
</div><!-- /.main-content -->

@stop